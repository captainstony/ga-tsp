import numpy.random

import Reporter
import random
import numpy as np

import matplotlib.pyplot as plt
import timeit

distance_matrix = None

# CONFIG
population_size = 100  # lambda
mutation_probability = 0.05  # alpha
offspring_size = 80  # mu
k_tournament = 3  # K-Tournament Selection
stop_crit_perc = 0.001  # percentage verschil waarbij we stoppen
stop_crit_num = 6 # aantal keer percentage verschil waarbij we stoppen

# DEBUG
nb_test = 10


# Modify the class name to match your student number.
class r0123456:

    def __init__(self):
        self.reporter = Reporter.Reporter(self.__class__.__name__)

    # The evolutionary algorithm's main loop
    def optimize(self, filename):
        # Read distance matrix from file.
        its = []
        means = []
        bests = []
        start = timeit.default_timer()
        for i in range(0, nb_test):
            file = open(filename)
            global distance_matrix
            distance_matrix = np.loadtxt(file, delimiter=",")
            file.close()

            # Your code here.
            ts = TravelingSalesman(population_size)
            no_improvement_counter = 0
            mean_objective_old = 0
            iteration = 0
            its.append([])
            means.append([])
            bests.append([])


            #while no_improvement_counter < stop_crit_num:  # yourConvergenceTestsHere
            while iteration < 250:
                # Your code here.
                ts.variate()
                ts.eliminate()

                if ts.mean_objective()*(1 + stop_crit_perc) > mean_objective_old:
                    no_improvement_counter += 1
                else:
                    no_improvement_counter = 0
                mean_objective_old = ts.mean_objective()
                iteration += 1

                print("iteratie:", iteration, "impr:", no_improvement_counter)
                print(ts.best_objective())
                print(ts.mean_objective())
                its[i].append(iteration)
                means[i].append(ts.mean_objective())
                bests[i].append(ts.best_objective())

                # Call the reporter with:
                #  - the mean objective function value of the population
                #  - the best objective function value of the population
                #  - a 1D numpy array in the cycle notation containing the best solution
                #    with city numbering starting from 0
                time_left = self.reporter.report(ts.mean_objective(), ts.best_objective(), ts.best_solution())

                if time_left < 0:
                    break
        stop = timeit.default_timer()

        m = min(map(len, its))
        min_bests = []
        min_means = []
        for i in range(0,nb_test):
            min_bests.append(bests[i][0:m])
            min_means.append(means[i][0:m])

        for i in range(0,nb_test):
            plt.plot(its[i][0:m], means[i][0:m], color='red', alpha=0.5)
            plt.plot(its[i][0:m], bests[i][0:m], color='blue', alpha=0.5)

        std_dev_means = np.std(min_means, axis=0)
        std_dev_bests = np.std(min_bests, axis=0)

        mean_means = np.array(min_means).mean(axis = 0)
        mean_bests = np.array(min_bests).mean(axis = 0)
        plt.plot(range(0,m), mean_means, color='red', linewidth=5.0, label="Mean objective function")
        plt.plot(range(0,m), mean_bests, color='blue', linewidth=5.0, label="Best objective funtion")
        plt.fill_between(range(0,m), mean_means+std_dev_means, mean_means-std_dev_means, facecolor='red', alpha=0.3)
        plt.fill_between(range(0,m), mean_bests+std_dev_bests, mean_bests-std_dev_bests, facecolor='blue', alpha=0.3)

        plt.xlabel("Iterations")
        plt.ylabel("Objective value")
        plt.title("Group phase GA performance")
        time = stop - start
        plt.text(0.84, 0.7, ' population size={} \n alpha={} \n mu={} \n k tournament={} \n runtime={} seconds \n end mean value={} \n end best value={}'.format(population_size, mutation_probability, offspring_size, k_tournament, round(time, 2), round(mean_means[-1],2), round(mean_bests[-1],2)), transform=plt.gca().transAxes) # monster regel
        plt.legend()
        plt.show()
        return 0


class TravelingSalesman:

    # initialisation
    def __init__(self, population_size):
        self.population = np.array([])
        for i in range(0, population_size):
            self.population = np.append(self.population, Path(len(distance_matrix)))
        #print(self.select())

    # TODO: is een hoge fitness beter of slechter? moet je in reverse=T/F aanpassen
    def k_tournament(self):
        p = random.choices(self.population, k=k_tournament)
        p = sorted(p, key=lambda path: path.fitness(), reverse=True)
        return p[0]

    def k_tournament_inverse(self):
        p = random.choices(self.population, k=k_tournament)
        p = sorted(p, key=lambda path: path.fitness(), reverse=False)
        return p[0]

    def select(self):
        new_population = np.array([])
        population = self.population.copy()
        for i in range(0, offspring_size):
            best = self.k_tournament()
            new_population = np.append(new_population, best)
            # Optimalisatie: verwijder alleen het 1e voorkomen, dus niet door heel de lijst gaan met np.where
            population = np.delete(population, np.where(population == best))
        return new_population

    # select
    # crossover
    # mutate (met bepaalde kans)
    def variate(self):
        selected = self.select()
        # Je kan geen step=2 gebruiken in een range functie
        for i in range(0, len(selected), 2):
            self.population = np.append(self.population, selected[i].crossover(selected[i + 1]))
        for i in range(len(self.population)):
            self.population[i].mutate()
        pass

    def eliminate(self): # beter slechtste weggooien dan beste bijhouden
        new_population = np.array([])
        for i in range(0, population_size):
            best = self.k_tournament_inverse()
            new_population = np.append(new_population, best)
            # Optimalisatie: verwijder alleen het 1e voorkomen, dus niet door heel de lijst gaan met np.where
            self.population = np.delete(self.population, np.where(self.population == best))
        self.population = new_population

    # def eliminate2(self): # beter slechtste weggooien dan beste bijhouden
    #     new_population = np.array([])
    #     for i in range(0, int(offspring_size/2)):
    #         worst = self.k_tournament()
    #         # Optimalisatie: verwijder alleen het 1e voorkomen, dus niet door heel de lijst gaan met np.where
    #         self.population = np.delete(self.population, np.where(self.population == worst))

    def mean_objective(self):
        sum = 0
        for path in self.population:
            sum += path.fitness()
        return sum / self.population.size

    def best_objective(self):
        best = np.Inf
        for path in self.population:
            if path.fitness() < best:
                best = path.fitness()
        return best

    def best_solution(self):
        best = np.Inf
        best_path = None
        for path in self.population:
            if path.fitness() < best:
                best = path.fitness()
                best_path = path
        return best_path.path  # beetje omslachtig maar werkt niet anders


class Path:
    # Representation:
    #     Cycle with first position always 0
    def __init__(self, num_cities, path=None):
        if path is None:
            path = np.random.permutation(num_cities)
            path = path[path != 0]
            self.path = np.insert(path, 0, 0)
        else:
            assert path[0] == 0
            assert num_cities == path.size
            self.path = path

    def __repr__(self):
        return 'Path: {}'.format(self.path)

    def __le__(self, other):
        return self.fitness() < other.fitness

    def set_path(self, path):
        self.path = path

    def cycle_notation(self):
        return self.path.copy()

    def adjacency_notation(self):
        length = len(self.path)
        adjacency = np.zeros(length, dtype=int)
        for i in range(length):
            j = np.where(self.path == i)[0][0]
            if j == length - 1:
                adjacency[i] = 0
            else:
                adjacency[i] = self.path[j + 1]
        return adjacency

    # input is gewoon een np array in adjacency notation
    def adjacency_to_cycle(self, adj):
        length = len(self.path)
        cycle = np.zeros(length, dtype=int)
        cycle[0] = 0
        for i in range(1, length):
            cycle[i] = adj[cycle[i - 1]]
        return cycle

    def fitness(self):
        fitness = 0
        for i in range(len(self.path) - 1):
            fitness += distance_matrix[self.path[i], self.path[i + 1]]
        return fitness + distance_matrix[self.path[-1], 0]

    def mutate(self):
        if random.uniform(0, 1) < mutation_probability:
            idx = range(len(self.path))
            i1, i2 = random.sample(idx[1:], 2)
            self.path[i1], self.path[i2] = self.path[i2], self.path[i1]

    '''
        def crossover_adj(self, other):
            self_adj = self.adjacency_notation()
            other_adj = other.adjacency_notation()

            length = len(self_adj)
            common_positions = []

            # self Fill common_positions array with tuples (index, value)
            for i in range(length):
                if self_adj[i] == other_adj[i]:
                    common_positions.append((i, self_adj[i]))

            # Initialise results array with common values
            result = np.zeros(length, dtype=int)
            for pos in common_positions:
                result[pos[0]] = pos[1]

            # Create array of missing values by taking the difference of the array [0, 1, ..., len-1] and the results array
            missing_values = np.setdiff1d(np.arange(0, length), result)

            # Add missing values randomly
            for i in range(length):
                pass
    '''

    def crossover(self, other):
        length = len(self.path)
        crossover_point = np.random.randint(1, length)
        result = self.path[:crossover_point]
        missing_values = np.setdiff1d(np.arange(1, length), result)
        for val in other.path:
            if val in missing_values:
                result = np.append(result, val)
        return Path(length, result)

'''
    def crossover_cycle(self, other):
        length = len(self.path)
        common_positions = []

        # Fill common_positions array with tuples (index, value)
        for i in range(length):
            if self.path[i] == other.path[i]:
                common_positions.append((i, self.path[i]))

        # Initialise result array with common values
        result = np.zeros(length, dtype=int)
        for pos in common_positions:
            result[pos[0]] = pos[1]

        # Create array of missing values by taking the difference of the array [1, 2, ..., len-1] and the results array
        # 0 can never be missing as it is always the first element
        missing_values = np.setdiff1d(np.arange(1, length), result)

        # Add missing values randomly
        for i in range(1, length):
            if result[i] == 0:
                value_array = np.random.choice(missing_values, 1)
                missing_values = np.setdiff1d(missing_values, value_array)
                result[i] = value_array[0]

        return result
'''

r = r0123456()
r.optimize('tour29.csv')
