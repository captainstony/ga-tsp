import Reporter
import numpy as np
import timeit
import random
import curses

# DEBUG
runtime = 5*60
#TODO: FIX UW FITNESS FUNCTION!!!!
# GA
numberOfMigrating = 25
numberOfGa = 2
migrateIter = 23
gaList = []

fitnessDict = {}
possibleNextNodeDict = {}

class r0735128:
    def __init__(self):
        self.reporter = Reporter.Reporter(self.__class__.__name__)

    def optimize(self, filename):
        start = timeit.default_timer()
        # NOTE: offspringSize moet kleiner zijn dan de helft van popSize, anders ga je problemen krijgen met de kTournamentWithRemove

        popSize = 150
        for i in range(0, numberOfGa):
            ga = GA(
                    filename = filename,
                    populationSize = popSize,
                    mutationProbability = 0.5,
                    offspringSize = int(popSize/2),
                    kTournament = 3,
                    initGreedyPaths = 5,
                    fractionSimilarDistance = 1/3,

                    elitism = True,
                    selection = i%3,
                    )
            gaList.append(ga)


        itr = 1
        print("Done init")
        while( start + runtime - timeit.default_timer() > 0 ):
            #GA
            for ga in gaList:
                ga.tick()

            if(itr % migrateIter == 0):
                fractionTheSame = 0
                for ga in gaList:
                    same = (len(ga.populationMatrix) - len(np.unique(ga.populationMatrix, axis=0)))
                    if(same != 0):
                        fractionTheSame += same / len(ga.populationMatrix)
                fractionTheSame = fractionTheSame/len(gaList)
                print("Fraction the same: " + str(fractionTheSame))

                # Populatie moet divers genoeg zijn voor de merge
                if(fractionTheSame < 0.05):
                    bestList = []
                    bestSelectList = []
                    worstList = []
                    remList = []

                    # TODO: kan beter
                    for ga in gaList:
                        bestList.append([])
                        worstList.append([])
                        remList.append([])

                    # Maak lijst van beste 
                    for i in range(0, numberOfMigrating):
                        for j in range(0, len(gaList)):
                            ga = gaList[j]

                            best = ga.kTournamentWithRemove(remList[j])
                            bestList[j].append(best)
                            remList[j].append(best)

                    # Maak lijst van slechtste
                    for i in range(0, numberOfMigrating):
                        for j in range(0, len(gaList)):
                            ga = gaList[j]

                            worst = ga.kTournamentWithRemove(remList[j], True)
                            worstList[j].append(worst)
                            remList[j].append(worst)
                    
                    # Sla de rijen op die gaan veranderen
                    for i in range(0,len(gaList)):
                        ga = gaList[i]
                        bestSelectList.append( np.array(ga.populationMatrix[bestList[i]].copy()) )

                    # Verwijder de slechte
                    for i in range(0,len(gaList)):
                        ga = gaList[i]
                        ga.populationMatrix = np.delete(ga.populationMatrix, worstList[i], axis=0)

                    # Append de beste nieuwe
                    for i in range(0,len(gaList)):
                        ga = gaList[i]
                        choice = np.random.choice( np.delete(np.arange(len(bestSelectList)), i) )
                        ga.populationMatrix = np.vstack(( 
                            ga.populationMatrix,
                            bestSelectList[choice]
                            ))

                    print("MIGRATE DONE")
                
            #TODO: neem de beste van de lijst
            mean = gaList[0].meanObjective()
            best = gaList[0].bestObjective()
            bestSol = gaList[0].bestSolution()
            timeLeft = self.reporter.report(mean, best, bestSol)

            #DEBUG
            for i in range(0,len(gaList)):
                ga = gaList[i]
                print("--GA"+str(i)+"--")
                print("best: " + str(ga.bestObjective()))
                print("mean: " + str(ga.meanObjective()))
                print("doub: " + str(len(ga.populationMatrix) - len(np.unique(ga.populationMatrix, axis=0))))
                print("sele: " + str(ga.selection))

            print("-------=" + str(itr) + "=-------")
            print("")
            itr+=1
            if timeLeft < 0:
                break

        #return 0
class GA:
    populationSize = 0 # lambda
    mutationProbability = 0 # alpha
    offspringSize = 0 # mu
    kTournament = 0 # K-Tournament Selection
    initGreedyPaths = 0
    fractionSimilarDistance = 0
    elitism = False

    distanceMatrix = None
    populationMatrix = None
    numCities = 0

    prevBest = []
    prevMean = []
    scr = None
    def __init__(self, filename, populationSize, mutationProbability, offspringSize, kTournament, initGreedyPaths, elitism, selection, fractionSimilarDistance=0):
        self.populationSize = populationSize
        self.mutationProbability = mutationProbability
        self.offspringSize = offspringSize
        self.kTournament = kTournament
        self.initGreedyPaths = initGreedyPaths
        self.fractionSimilarDistance = fractionSimilarDistance
        self.elitism = elitism

        self.selection = selection

        file = open(filename)
        self.distanceMatrix = np.loadtxt(file, delimiter=",")
        file.close()
        self.numCities = len(self.distanceMatrix)

        global possibleNextNodeDict
        for i in range(0, self.numCities):
            possibleNextNodeDict[i] = np.where((self.distanceMatrix[i] != np.inf) & (self.distanceMatrix[i] != 0))[0]

        self.initialisation()

    def printConfig(self):
        print("pop size:", self.populationSize)
        print("off size:", self.offspringSize)
        print("mutation:", self.mutationProbability)
        print("k tourna:", self.kTournament)
        print("elitism :", self.elitism)
        print("Selection:")
        if self.selection == 0:
            print("distance")
            print("frac dis:", self.fractionSimilarDistance)
        elif self.selection == 1:
            print("roulette")
        else:
            print("classic")

    def tick(self):
        # GA

        # Elitism, hou de beste bij
        self.bestInd = 0
        if self.elitism:
            bestSol = self.calculateFitness(self.bestInd)
            for i in range(1, len(self.populationMatrix)):
                v = self.calculateFitness(i)
                if (v < bestSol):
                    self.bestInd = i
                    bestSol = v

        '''
        if self.selection == 0:
            selected = self.selectDistance()
        elif self.selection == 1:
            selected = self.selectRoulette()
        else:
            selected = self.selectClassic()
        '''
        selected = self.selectClassic()
            
        self.variate(selected)
        self.localSearch()

        #if(bestSol not in self.populationMatrix):
        #    self.populationMatrix = np.vstack(( self.populationMatrix, bestSol ))
        
        self.eliminate()

    #TODO: dit kunt ge cachen!
    def meanObjective(self):
        means = []
        for i in range(len(self.populationMatrix)):
            means.append(self.calculateFitness(i))
        return np.mean(np.array(means))

    def bestObjective(self):
        vals = []
        for i in range(len(self.populationMatrix)):
            vals.append(self.calculateFitness(i))
        return np.min(np.array(vals))


    def bestSolution(self):
        ind = 0
        best = self.calculateFitness(ind)
        for i in range(1, len(self.populationMatrix)):
            v = self.calculateFitness(i)
            if (v < best):
                ind = i
                best = v
        return self.populationMatrix[ind]
               
    def initialisation(self):
        per = self.generateRandomPath()
        self.populationMatrix = np.array(per)
        while(True):
            per2 = self.generateRandomPath()
            if(any(np.equal(per, per2))):
                self.populationMatrix = np.vstack(( self.populationMatrix, per2 ))
                break

        for i in range(0,self.populationSize):
            while(True):
                per = self.generateRandomPath()
                if( not any(np.equal(self.populationMatrix,per).all(1))):
                    self.populationMatrix = np.vstack(( self.populationMatrix, per ))
                    break
                
    def generateRandomPath(self):
        paths = []
        while len(paths) < self.initGreedyPaths:
            per = np.append(0,np.random.permutation(range(1,self.numCities)))
            while self.calculateFitnessPath(per) == np.inf:
                per = np.append(0,np.random.permutation(range(1,self.numCities)))
            paths.append(per)
        best = paths[0]
        for p in paths[1:]:
            if self.calculateFitnessPath(p) < self.calculateFitnessPath(best):
                best = p

        return best

    def selectClassic(self):
        newPop = []
        i=0
        while(i < self.offspringSize):
            best_i = self.kTournamentWithRemove(newPop)
            if(not best_i in newPop):
                i+=1
                newPop.append(best_i)

        return newPop

    def selectRoulette(self):
        newPop = []
        som = 0
        for i in range(0, len(self.populationMatrix)):
            som += self.calculateFitness(i)
        number = random.uniform(0,1)
        for i in range(0, len(self.populationMatrix)):
            p = (1/(len(self.populationMatrix) - 1)) * (1 - (self.calculateFitness(i) / som))
            if(number > p):
                newPop.append(i)
        return newPop

    def selectDistance(self):
        newPop = []
        remPop = []
        i=0
        while(i < self.offspringSize):
            exclude = list(set(newPop + remPop))
            best_i = self.kTournamentWithRemove(exclude)
            if(not best_i in newPop):
                numbers = np.array(range(0, len(self.populationMatrix)))
                numbers = np.delete(numbers, exclude)
                i+=1
                newPop.append(best_i)
                for j in numbers:
                    if(best_i != j):
                        d = self.distance(best_i,j)
                        if(d < self.fractionSimilarDistance * self.numCities):
                            remPop.append(j)
                            break

        return newPop

    def variate(self, selected):
        for i in range(0, len(selected)-1, 2):
            #cross = self.crossoverBestBCSCX(selected[i], selected[i+1])
            cross = self.crossoverBCSCX(selected[i], selected[i+1])
            #cross = self.crossoverOX(selected[i], selected[i+1])
            assert self.calculateFitnessPath(cross) != np.inf
            if( not any(np.equal(self.populationMatrix, cross).all(1))):
                self.populationMatrix = np.vstack(( self.populationMatrix, cross ))
        for i in range(len(self.populationMatrix)):
            # Elitism, hou de beste bij
            if self.elitism and self.bestInd != i:
                if(random.uniform(0,1) < self.mutationProbability):
                    self.mutateSwap(i)
            assert self.calculateFitness(i) != np.inf
    
    #TODO: kan inf paden maken
    def localSearch(self):
        for i in range(0, self.populationSize):
            p = self.populationMatrix[i].copy()
            for k in range(1, np.random.randint(1,5)):
                self.mutateSwap(i)
            if(self.calculateFitness(i) > self.calculateFitnessPath(p)):
                self.populationMatrix[i] = p


    def eliminateRetry(self):
        newPop = []
        i=0
        while(i < self.populationSize):
            #Als dit pad al in de nieuwe populatie zit, dan skip je
            s = self.kTournament()
            if(not s in newPop):
                i+=1
                newPop.append(s)
        self.populationMatrix = self.populationMatrix[newPop]


    def eliminateList(self):
        newPop = []
        i=0
        while(i < self.populationSize):
            idx = random.sample(range(0, len(self.populationMatrix)), self.kTournament)
            best = sorted(idx, key=lambda i: self.calculateFitness(i), reverse=False)
            for s in best:
                if(not s in newPop):
                    i+=1
                    newPop.append(s)
                    break
        self.populationMatrix = self.populationMatrix[newPop]

    def eliminate(self):
        newPop = []
        i=0
        while(i < self.populationSize):
            best = self.kTournamentWithRemove(newPop)
            if(not best in newPop):
                i+=1
                newPop.append(best)
        self.populationMatrix = self.populationMatrix[newPop]


    #TODO: Kan inf paden maken
    def crossoverOX(self, i1, i2):
        p = self.populationMatrix[i1]
        q = self.populationMatrix[i2]
        cross = np.empty(self.numCities);
        cross.fill(-1)

        idx = range(self.numCities)
        j1 = np.random.randint(1,self.numCities)
        j2 = np.random.randint(j1,self.numCities)
        for t in range(j1,j2):
            cross[t] = p[t]

        for t in range(0, j1):
            k=0
            while q[k] in cross:
                k+=1
            cross[t] = q[k]
        for t in range(j2, self.numCities):
            k=0
            while q[k] in cross:
                k+=1
            cross[t] = q[k]
        return cross


    def crossoverBCSCX(self,i1, i2):
        p = self.populationMatrix[i1]
        q = self.populationMatrix[i2]
        newPath = [0]
        while len(newPath) < self.numCities:
            possible = []
            #TODO: hier kan je sneller breaken
            for j in range(0, self.numCities):
                if p[j] == newPath[-1]:
                    if p[j-1] not in newPath and p[j-1] in possibleNextNodeDict[newPath[-1]]:
                        possible.append(p[j-1])

                    if j+1 < self.numCities:
                        if p[j+1] not in newPath and p[j+1] in possibleNextNodeDict[newPath[-1]]:
                            possible.append(p[j+1])
                    else:
                        if p[0] not in newPath and p[0] in possibleNextNodeDict[newPath[-1]]:
                            possible.append(p[0])

                if q[j] == newPath[-1]:
                    if q[j-1] not in newPath and q[j-1] in possibleNextNodeDict[newPath[-1]]:
                        possible.append(q[j-1])

                    if j+1 < self.numCities:
                        if q[j+1] not in newPath and q[j+1] in possibleNextNodeDict[newPath[-1]]:
                            possible.append(q[j+1])
                    else:
                        if q[0] not in newPath and q[0] in possibleNextNodeDict[newPath[-1]]:
                            possible.append(q[0])

            if len(possible) != 0:
                best = possible[0]
                for j in possible[1:]:
                    if(self.distanceMatrix[newPath[-1], j] < self.distanceMatrix[newPath[-1], best]):
                        best = j

                newPath.append(best)
            else:
                np.random.shuffle(possibleNextNodeDict[newPath[-1]])
                rand = possibleNextNodeDict[newPath[-1]]
                best = rand[0]
                if best == 0:
                    best = rand[1]
                if best in newPath:
                    for n in rand:
                        #TODO: kan beter
                        if(n not in newPath and best != 0):
                            best = n
                            break
                newPath.append(best)

        return newPath 


    def crossoverBestBCSCX(self,i1, i2):
        p = self.populationMatrix[i1]
        q = self.populationMatrix[i2]
        newPath = [0]
        while len(newPath) < self.numCities:
            possible = []
            #TODO: hier kan je sneller breaken
            for j in range(0, self.numCities):
                if p[j] == newPath[-1]:
                    if p[j-1] not in newPath and p[j-1] in possibleNextNodeDict[newPath[-1]]:
                        possible.append(p[j-1])

                    if j+1 < self.numCities:
                        if p[j+1] not in newPath and p[j+1] in possibleNextNodeDict[newPath[-1]]:
                            possible.append(p[j+1])
                    else:
                        if p[0] not in newPath and p[0] in possibleNextNodeDict[newPath[-1]]:
                            possible.append(p[0])

                if q[j] == newPath[-1]:
                    if q[j-1] not in newPath and q[j-1] in possibleNextNodeDict[newPath[-1]]:
                        possible.append(q[j-1])

                    if j+1 < self.numCities:
                        if q[j+1] not in newPath and q[j+1] in possibleNextNodeDict[newPath[-1]]:
                            possible.append(q[j+1])
                    else:
                        if q[0] not in newPath and q[0] in possibleNextNodeDict[newPath[-1]]:
                            possible.append(q[0])

            if len(possible) != 0:
                best = possible[0]
                for j in possible[1:]:
                    if(self.distanceMatrix[newPath[-1], j] < self.distanceMatrix[newPath[-1], best]):
                        best = j

                newPath.append(best)
            else:
                best = possibleNextNodeDict[newPath[-1]][0]
                for n in possibleNextNodeDict[newPath[-1]][1:]:
                    #TODO: kan beter
                    if((best in newPath) and (n not in newPath)):
                        best = n
                    else:
                        if((n not in newPath) and (self.distanceMatrix[newPath[-1], n] < self.distanceMatrix[newPath[-1], best])):
                            best = n
                newPath.append(best)

        return newPath 


    def kTournament(self, reverse=False):
        best = np.random.randint(0,len(self.populationMatrix))
        for i in np.random.randint(0,len(self.populationMatrix), size=self.kTournament):
            if(reverse):
                if( self.calculateFitness(i) > self.calculateFitness(best) ):
                    best = i
            else:
                if( self.calculateFitness(i) < self.calculateFitness(best) ):
                    best = i
        return best

    def kTournamentWithRemove(self, removeList , reverse=False):
        numbers = np.array(range(0, len(self.populationMatrix)))
        numbers = np.delete(numbers, removeList)
        
        best = np.random.choice(numbers)
        for ind in np.random.choice(numbers , size=self.kTournament):
            if(reverse):
                if( self.calculateFitness(ind) > self.calculateFitness(best) ):
                    best = ind
            else:
                if( self.calculateFitness(ind) < self.calculateFitness(best) ):
                    best = ind
        return best
    
    def distance(self, i, j):
        dist = self.numCities
        for t in range(0,self.numCities):
            if self.populationMatrix[i][t] == self.populationMatrix[j][t]:
                dist -= 1
        return dist

    def mutateSwap(self, i):
        og = self.populationMatrix[i].copy()
        idx = range(self.numCities)
        j1, j2 = random.sample(idx[1:],2)
        p = og.copy()
        p[j1], p[j2] = p[j2], p[j1]
        while self.calculateFitnessPath(p) == np.inf:
            idx = range(self.numCities)
            j1, j2 = random.sample(idx[1:],2)
            p = og.copy()
            p[j1], p[j2] = p[j2], p[j1]
            self.populationMatrix[i] = p

    #TODO: sla geen paden op die inf als afstand hebben
    def calculateFitness(self, i):
        global fitnessDict
        h = hash(tuple(self.populationMatrix[i]))
        if(h in fitnessDict):
            return fitnessDict[h]

        fitness = self.distanceMatrix[self.populationMatrix[i,self.numCities-1], self.populationMatrix[i,0]]
        for j in range(self.numCities - 1 ):
            val = self.distanceMatrix[self.populationMatrix[i,j], self.populationMatrix[i,j+1]]
            if val == np.inf:
                fitness = np.inf
                break
            fitness += val

        fitnessDict[h] = fitness
        return fitness

    def calculateFitnessPath(self, p):
        global fitnessDict
        h = hash(tuple(p))
        if(h in fitnessDict):
            return fitnessDict[h]

        fitness = self.distanceMatrix[int(p[self.numCities-1]), int(p[0])]
        for j in range(self.numCities - 1 ):
            val = self.distanceMatrix[int(p[j]), int(p[j+1])]
            if val == np.inf:
                fitness = np.inf
                break
            fitness += val

        fitnessDict[h] = fitness
        return fitness


r = r0735128()
r.optimize('tour29.csv')
