\documentclass[a4paper,10pt]{article}
\usepackage[utf8]{inputenc}

\usepackage[english]{babel}
\usepackage[dvinames]{xcolor}
\usepackage[compact,small]{titlesec}
\usepackage{booktabs}
\usepackage{multirow}
\usepackage{amsfonts,amsmath,amssymb}
\usepackage{marginnote}
\usepackage[top=1.8cm, bottom=1.8cm, outer=1.8cm, inner=1.8cm, heightrounded, marginparwidth=2.5cm, marginparsep=0.5cm]{geometry}
\usepackage{enumitem}
\setlist{noitemsep,parsep=2pt}
\newcommand{\highlight}[1]{\textcolor{kuleuven}{#1}}
\usepackage{pythonhighlight}
\usepackage{cleveref}
\usepackage{graphicx}
\usepackage{float} 

\newcommand{\nextyear}{\advance\year by 1 \the\year\advance\year by -1}
\newcommand{\thisyear}{\the\year}
\newcommand{\deadlineCode}{December 31, \thisyear{} at 16:00 CET}
\newcommand{\deadlineReport}{\deadlineCode}

\newcommand{\ReplaceMe}[1]{{\color{blue}#1}}
\newcommand{\RemoveMe}[1]{{\color{purple}#1}}

\setlength{\parskip}{5pt}

%opening
\title{Evolutionary Algorithms: Final report}
\author{Tom Van Steenbergen (r0735128)}

\begin{document}
\fontfamily{ppl}
\selectfont{}

\maketitle

\iffalse
\section{\RemoveMe{Formal requirements}} \label{sec_this}

\RemoveMe{The report is structured for fair and efficient grading of over 100 individual projects in the space of only a few days. Please respect the exact structure of this document. You are allowed to remove sections \ref{sec_this} and \ref{sec_other}. Brevity is the soul of wit: a good report will be \textbf{around $7.5$ pages} long. The hard limit is 12 pages. 

    \begin{quote}
        Think of this report as a \textbf{take-home exam}; it will be used at the exam for structuring the discussion and questions. Make an effort so that it can be visually scanned efficiently, e.g., by using boldface or colors to highlight key points, using lists, clearly defined paragraphs, figures, etc.

        You do not need to explain in this report \textbf{how} the techniques and concepts that are literally in the slides work. The goal of this report is \textbf{not} to illustrate that you can reproduce the slides. You need to convince me that you aptly used these (and other) techniques in this project. If I have doubts about your understanding of certain concepts in the course materials, I will test this hypothesis at the exam.
    \end{quote}

    It is recommended that you use this \LaTeX{} template, but you are allowed to reproduce it with the same structure in a WYSIWYG-editor. The purple text containing our evaluation criteria can be removed. You should replace the blue text with your discussion.

This report should be uploaded to Toledo by \deadlineReport. It must be in the \textbf{Portable Document Format} (pdf) and must be named \texttt{r0123456\_final.pdf}, where r0123456 should be replaced with your student number.}

\fi
\section{Metadata}

\begin{itemize}
    \item \textbf{Group members during group phase:} Andreas Awouters, Hanne Christiaensen, Nina Claessens and Tom Van Steenbergen
    \item \textbf{Time spent on group phase:} 2 hours
    \item \textbf{Time spent on final code:} 60 hours
    \item \textbf{Time spent on final report:} 11 hours
\end{itemize}

\section{Peer review reports (target: 1 page)}

\subsection{The weak points}
Weak points identified in the peer review reports:

\begin{enumerate}
    \item \textbf{Biased recombination operator} It will always keep the first part of the first parent. The second parent does not have a lot of influence.
    \item k-Tournament uses fitness as evaluation criteria. This is \textbf{ a computationally heavy operation }.
    \item \textbf{No stopping criterion.} We used the full 5 minutes, even if there was no improvement.
    \item \textbf{Fixing node 0 in the representation.} This limits for other operators. Eg, crossover and mutation.
    \item \textbf{Swap mutation is destructive.} Changing two nodes implies changing four edges.
\end{enumerate}

\subsection{The solutions}
Solutions for previous problems:

\begin{enumerate}
    \item \textbf{Recombination operator} was replaced with modified version of \textbf{BCSCX} = (Bidirectional Circular Sequential Constructive Crossover)
    \item The representation was modified so that \textbf{0 is not fixed in place.}
    \item Swap mutation was replaced with RSM = (\textbf{Reverse Sequence Mutation}). This mutation reverses sublists.
    \item The main algorithm creates a new instance of the genetic algorithm after convergence has been reached. This new instances used the populations of previous instances to reach better solutions.
\end{enumerate}

Weak points that have not been solved.

\begin{enumerate}
    \item \textbf{This representation}, fixing the 0 in place, may cause a lot of \textbf{duplicate paths}. These paths are hard to recognize because they might be rolled.
    \item k-Tournament still uses fitness for evaluation. But the computation time has been decreased by using the \textbf{numba library}. This library compiles designated functions.
\end{enumerate}

\subsection{The best suggestion}

\textbf{Replacing the crossover operator.} 
After reading some papers on the TSP, I noticed that a lot of the research was done over the crossover operator.
Using \textbf{BCSCX = Bidirectional Circular Sequential Constructive Crossover} improved the offspring quality by a lot.
But the operation was costly.
This was solved by using the numba library.
Which compiles the function.

\section{Changes since the group phase (target: $0.5$ pages)} 

A lot of changes have happened since the group phase.
These are the main changes to the algorithm since the group phase:
\begin{enumerate}
    \item In the group phase we used python objects, this was replaced with a numpy matrix.
        This improved performance, because matrix operations are faster than python object operations.
    \item The initialisation has greedy mechanisms, and is recursive. (section \ref{initialisation})
        Starting with a good population saves some time in the first iterations.
        But making iterations is still better than trying to start with a good initial population.
    \item The mutation probability is dynamic. (section \ref{mutation})
        Changing the mutation probability depending on the population improves diversity.
    \item The mutation operator has changed. (section \ref{mutation})
    \item The crossover operator changed to BCSCX. (section \ref{recombination})
    \item The stop condition has changes to a "leaky bucket" model. (section \ref{stop})
    \item The main algorithm creates multiple genetic algorithm instances. (section \ref{diversity})
    \item Implemented a local search step (section \ref{local})
    \item Using the numba library, this increased the performance by 300\% in some cases.
\end{enumerate}

\section{Final design of the evolutionary algorithm (target: $3.5$ pages)} 

\subsection{The three main features}

The three main components of the algorithm:
\begin{enumerate}
    \item BCSCX as crossover (in section \ref{recombination}).
        BCSCX is good at merging different optimal paths of the parents.
    \item Optimal local search (in section \ref{local}).
        This operator takes the most amount of time.
        It transforms paths into the optimal paths over the RSM function.
    \item Migration between instances (in section \ref{diversity}).
        This is the main diversity promotion mechanism, without this the algorithm would be too much local search.
\end{enumerate}

\subsection{The main loop}

\begin{figure}[H]
    \includegraphics[width=\linewidth]{chart}
    \caption{Flow of the genetic algorithm}
    \label{fig:chart}
\end{figure}

\subsection{Representation} \label{representation}

\textbf{Adjacency notation} is used for the representation of a path.
A node is not fixed in place over all possible paths. 
Doing this allows for fast recognition of double paths in the population, but limits the possibilities for operators.
Eg, you can't implement some crossover functions.
This is probably the easiest representation you can think of, and a lot of papers about solving TSP with GA uses this notation.
\\
The population matrix is a numpy array, meaning the paths are numpy arrays.
\\
Another possible notation is the cycle notation.
But the biggest drawback is that you have to check for cycles in every path you generate.

\subsection{Initialization} \label{initialisation}

Initialization is a much harder problem than it seems.
This is because the problem we are trying to solve is \textbf{ATSP with infinite values.}
Meaning, the distance matrix in the TSP is not symmetric and can contain infinite values.
Generating \textbf{random paths might result in paths with infinite length.}
Paths that are infinite are useless, because the fitness is used as a score in k-Tournament.
\\
The algorithm generates random \textbf{greedy paths recursively.}
This is done by starting with a path, containing only one node.
The next node is picked using a greedy heuristic, meaning looking at the value in the distance matrix for the previous and current node.
When a node is selected, the function is called recursively, but with the new node appended to the input path.
When no possible nodes can be selected as a next node, the recursion backtracks, and selects the next best node.
This is done until a valid path is generated.
\\
The problem with this method is that it's $O(n^2)$.
After some experimentation, I found that doing iterations is better than spending time calculating a greedy initial population.
So I added one step before doing the recursive method.
textbf{Before calculating a path recursivly, generate a random one and check if it's valid.}

\subsection{Selection operators} \label{selection}

The selection operator used is \textbf{kTournament} with $k=3$.
I have tried other selection operators, such as a distance selection operator. 
This operator selects a paths based on kTournament, and removes a path that looks very similar.
But this operator was slow, and did not provide a huge benefit.
So this operator was not used.
Another operator was the roulette wheel operator. 
This selects paths based on how good they are compared to the rest of the paths in the population.
But how do you handle paths that have an infinite length?
You could remove them, but this does not promote diversity.
Experimenting with this selection operator, a lot of doubles were chosen.
Again, you could check for doubles, but this makes the operator rather slow.

\subsection{Mutation operators} \label{mutation}

The variate operator does the mutation. 
I use two kinds of mutation \textbf{swap mutation and RSM (Reverse Subsequence mutation)}.
RSM works as follows.
Select a random sublist of the path, and reverse this sublist in place.
Both are applied to a path, but the mutation with the best fitness is picked for reinsertion in the population.
\\
The mutation probability increases with the number of iterations that do not improve the best fitness in the population.
This can be seen as a last ditch attempt at improving before moving on.

\subsection{Recombination operators} \label{recombination}

The biggest problem with the recombination operator was creating paths that have an infinite length.
After researching some popular crossover operators, a lot of them could not be used in this context. 
Due to paths that might have infinite length.
You could mitigate this by skipping the crossover if this happens, but I thought that was a dirty solution.
You could also retry the crossover until something useful is produced.
\\
However, I implemented a modified version of \textbf{BCSCX (Bidirectional Circular Sequential Crossover)} so that it can handle infinite values.
\\
Let $p$ and $q$ be both the parent paths.
The crossover path, referred to as $cross$, is initialized with a random start node.
The next node is selected as follows.
Let $x=$ last node in $cross$.
Search for the neighbours of $x$ in $p$ and $q$.
Two nodes are neighbours if they are next to each other in the path.
This includes the connection from the last path to the beginning.
In my version of BCSCX, it searches for all the possible neighbours.
These are, if appending this neighbour to $cross$, the path length would not be infinite, and the neighbour is not already in $cross$.
Select from the remaining neighbours the one that has the smallest length from $x$ to the neighbour.
If there are no available neighbours, select randomly the next node.
This process is repeated until $cross$ is a full path.
\\

Another crossover I implemented was AEX = (Alternating edge crossover).
The path $cross$ starts with a random node.
Each iteration, the algorithm appends the right neighbour of $p$ or $q$.
AEX alternates between $p$ and $q$ in each iteration.
This crossover was fast, but did not have good results.
\\
I have tried using BCSCX with an arity greater than 2, but this did not improve the results.

\subsection{Elimination operators} \label{elimination}

I used \textbf{kTournament}, for the same reasons as in the selection operator section.

\subsection{Local search operators} \label{local}

For local search, I use the \textbf{optimal solution of the RSM (Reverse Sequence Mutation)}.
What this means is that the local search generates all possible sublists of a path.
Reversing these sublists in place and picking the mutation that has the best fitness.
Generating all sublists is $O(n^2)$, so doing this over all possible paths in the population will take to long.
So I only apply this local search to only 5\% of the population.
When the local search does not improve a path, another path is chosen.
This is so each iteration at least 5\% of the paths in the population are better compared to a previous iteration.
These paths are chosen randomly.

\subsection{Diversity promotion mechanisms} \label{diversity}

The genetic algorithm has two diversity promotion mechanisms.
\begin{itemize}
    \item \textbf{Migration between previous instances}
        Convergence is hard to measure.
        I chose for a "leaky bucket" strategy. 
        This strategy creates an empty "bucket", if the genetic algorithm does not improve for an iteration, it adds one to the "bucket".
        If the genetic algorithm does improve, it subtracts two from the "bucket".
        But do not go below zero.
        This means that in the initial stages of the algorithm, the "bucket" is empty or close to empty.
        This is because a lot of changes are happening.
        When time goes on, the "bucket" starts to fill.
        Until a maximum is reached.
        The maximum is determined through this function $log(numbCities + 1) * 70$. With \texttt{numbCities} the number of cities for the current problem.
        This is a dynamic value because a lot more iterations are needed in larger instances of this problem.
        But we do not want to scale too hard, that's why I chose for a logarithmic function.
        The parameters in this function are determined through experiments.
        Looking at when the genetic algorithm converges for different sizes of the problem.
        \\
        After the "bucket" is full, meaning we have reached convergence, the genetic algorithm will start another instance of itself.
        The population of the previous instance is kept in memory.
        After creating of this new instance, the algorithm will only calculate new iterations for the new instance.
        \\
        \textbf{When the "bucket" of this new instance is almost full ($3/4*bucketSize$), a migration step wil start.}
        The migration step combines the previous populations with the current one.
        Every previous population is randomly sampled.
        These paths are added to the current instance.
        After this step, the "bucket" is set to zero, only if this is the first migration this instance has undergone.
        I reset the "bucket", so the population has time to mix in the newly added paths.

    \item \textbf{Dynamic mutation probability}. 
        The mutation probability increases with how full the "bucket" is.

\end{itemize}

\subsection{Stopping criterion} \label{stop}

The algorithm stops if 5 instances of the genetic algorithm fail to improve the result.

\subsection{Parameter selection} \label{parameter}

I tried to optimize the parameters for a given dataset.
But each time I found a good set of parameters for this dataset, it would not work well on another one.
This is probably because of \textbf{overfitting}.
Determining these parameters before the execution of the actual genetic algorithm would take too long.
So I went for another approach.
\textbf{I created a set of possible parameters and ran the algorithm against the different datasets.}
Then I picked the best set of parameters.
This is not an optimal method, the set of parameters is still chosen by me.
\\
Parameters that are hand-picked:
\begin{enumerate}
    \item Population size
    \item Offspring size
    \item kTournament parameter
    \item Migrate point of bucket
    \item Fraction of population to apply local search
\end{enumerate}
Also, I made some \textbf{parameters dynamic.}
Like the mutation probability and the maximum bucket size.

\subsection{Other considerations}

\textbf{Elitism} is used in the algorithm.
\\
The numba library provides a \textbf{parallelisation} decorator.
I used this on the variate and local search operators.

\section{Numerical experiments (target: 1.5 pages)}
The convergence graphs are generated from data collected by 20 runs of the genetic algorithm.
In all cases except the \texttt{tour29.csv} dataset, the algorithms used all of the available time.
The individual runs are plotted with the small lines, while the mean of the runs are plotted using a thicker line.
Red represents the mean fitness of a population, blue represents the best fitness of a population.
\\
I also included the convergence graphs of the other datasets, because they show some interesting behaviour.

\subsection{Metadata}

Computer used for generating the numerical results:
\begin{itemize}
    \item CPU: Intel(R) Core(TM) i7-7500U CPU @ 2.70GHz
    \item Number of cores: 2
    \item RAM: 16 GB DDR4
    \item Python version: 3.9.7
\end{itemize}

Configuration of the genetic algorithm:
\begin{itemize}
    \item populationSize = 180
    \item offspringSize = 25
    \item kTournament value = 3
    \item maxBucketSize = $log(numbCities + 1) * 70$
    \item merge populations on current bucketSize = $3/4 * maxBucketSize$
    \item mutationProbability = $0.5 - (min(0.4, maxBucketSize - currentBucketSize/maxBucketSize))$
    \item fraction of population to optimize in local search = 5\%
    \item elitism = $True$
\end{itemize}


\subsection{tour29.csv}

The convergence graph in figure \ref{fig:tour29} shows how the genetic algorithm is designed.
It starts with an initial population and optimizes until it has converged.
After that, it creates a new instance that is completely independent of the previous one.
Then this instance is optimized, and merges with the previous population.
This is repeated until the stop condition is reached.
\\
Between every large spike, there is a small one.
This is the merge step.
\\
The convergence is rather quick, it only takes \textbf{one minute to reach the stop condition.}
\begin{figure}[H]
    \includegraphics[width=\linewidth]{../tour29.csv}
    \caption{Convergence graph for the tour29.csv dataset for 20 runs.}
    \label{fig:tour29}
\end{figure}

Best sequence: 
2-6-8-12-13-15-23-24-26-19-25-27-28-22-21-20-16-17-18-14-11-10-9-5-0-1-4-7-3
\\
Has the corresponding fitness: 27154.49
\\
Looking at both the convergence graph and the histograms, I can say with a high degree of confidence that this is the optimal solution for this dataset.
The histogram on figure \ref{fig:besthist} only contains one value.
This means that every instance of the algorithm converged to the same fitness.
They are all the same path.
The histogram on figure \ref{fig:meanhist} shows an almost normal distribution.
\begin{figure}[H]
    \includegraphics[width=\linewidth]{../besthist}
    \caption{Histogram of the best objective values for 1000 runs}
    \label{fig:besthist}
\end{figure}

\begin{figure}[H]
    \includegraphics[width=\linewidth]{../meanhist}
    \caption{Histogram of the mean objective values for 1000 runs}
    \label{fig:meanhist}
\end{figure}

Configuration:
\begin{itemize}
    \item maxBucketSize = $103$
    \item merge populations on current bucketSize = $77$
\end{itemize}

\subsection{tour100.csv}

That is why there are many more spikes in figure \ref{fig:tour100}, compared to the previous dataset (figure \ref{fig:tour29}).
This is true for all other datasets (except \texttt{tour29.csv}), so all available time was used.

\begin{figure}[H]
    \includegraphics[width=\linewidth]{../tour100.csv}
    \caption{Convergence graph for the tour100.csv dataset for 20 runs.}
    \label{fig:tour100}
\end{figure}

The best fitness found: 228637.20
\\
Configuration:
\begin{itemize}
    \item maxBucketSize = $140$
    \item merge populations on current bucketSize = $105$
\end{itemize}


\subsection{tour250.csv}

The algorithm only had time to make two instances of the genetic algorithm.
This can be seen in figure \ref{fig:tour250}.
The algorithm starts off and converges, this is around iteration 250.
After that, it creates another instance, this can be seen in the spike of the mean objective value.
Around iteration 770, this instance merges with the first one.
This is the second spike that can be seen.
This happens again around iteration 900.
\begin{figure}[H]
    \includegraphics[width=\linewidth]{../tour250.csv}
    \caption{Convergence graph for the tour250.csv dataset for 20 runs.}
    \label{fig:tour250}
\end{figure}
The best fitness found: 45733.51
\\
Configuration:
\begin{itemize}
    \item maxBucketSize = $168$
    \item merge populations on current bucketSize = $126$
\end{itemize}
\subsection{tour500.csv}

\textbf{The algorithm converged at iteration 100, but decided not to create another instance.}
This is because there was not enough time to converge the second one, the time would be wasted.
\\
The calculated solution is probably \textbf{not close to the optimal one.}
This is because the algorithm \textbf{only had time to create one instance, there was no merging step.}
In this case, the algorithm is a local search on the initial population.
The search space is huge, and the algorithm did not a lot of exploring.
\begin{figure}[H]
    \includegraphics[width=\linewidth]{../tour500.csv}
    \caption{Convergence graph for the tour500.csv dataset.}
    \label{fig:tour500}
\end{figure}
The best fitness found: 155096.93
\\
Configuration:
\begin{itemize}
    \item maxBucketSize = $189$
    \item merge populations on current bucketSize = $142$
\end{itemize}

\subsection{tour1000.csv}

Using larger datasets increases \textbf{computation exponentially, this is because of the local search operator.}
As can be seen on the number of iterations the algorithm computed (only 29 iteration).
Looking at figure \ref{fid:tour1000}, you can see that the algorithm can converge quite fast.
Only after 5 iterations, it has moved the mean objective value from $6*10^6$ too $10^6$.
After the initial progress, it gets slower.
\\
For the same reason as previous large datasets, the calculated solution is probably not close to the optimal one.


\begin{figure}[H]
    \includegraphics[width=\linewidth]{../tour1000.csv}
    \caption{Convergence graph for the tour1000.csv dataset for 20 runs.}
    \label{fig:tour1000}
\end{figure}

The best fitness found: 218597.02
\\
Configuration:
\begin{itemize}
    \item maxBucketSize = $210$
    \item merge populations on current bucketSize = $158$
\end{itemize}

\section{Critical reflection (target: 0.75 pages)}

Three strengths of an evolutionary algorithm:

\begin{enumerate}
    \item Evolutionary algorithms provide an approximation of the best solution.
        Most problems that are solved using an evolutionary algorithm are NP problems.
        Meaning, they take a long time to get an optimal solution.
    \item These algorithms are global optimization algorithms and scale well to higher dimensional problems.
        The theory of these algorithms are applicable to a lot of problems, without looking at the problem specifically.
        This means that an operator that work on a specific representation can be used on all problems using that representation.
    \item You do not need very good heuristics.

\end{enumerate}

Three main weak points of an evolutionary algorithm:

\begin{enumerate}
    \item Evolutionary algorithms do not guaranty an optimal solution.
        In most cases, you do not even know how far off the generated solution is to the optimal one.
    \item They are stochastic, meaning they use randomness.
        This means it is hard to reproduce results, and makes debugging difficult.
        Let's say you have found a new crossover operator.
        How would you know it is actually better than another one?
        You could test it on a dataset, but due to the randomness you would have to do a lot of tests.
        And look at the aggregate of these tests before drawing a conclusion whether your new crossover is better or not.
        Another problem is that the dataset might be better for your operator, but another dataset might not.
        In general, it is \textbf{hard to prove an operator is better} than another one.
    \item After finding a solution, you do not know if this is the best solution.
        It might be the case that the algorithm is stuck in a local minimum.
        
\end{enumerate}

If you come across a problem, you don't know how to solve, a genetic algorithm might be a good start.
It seems to me that the genetic algorithm theory is applicable to almost all problems.
This makes it very useful as a first attempt at solving a lot of problems.
But specialised algorithms are probably better at solving specific problems.
\\
Genetic algorithms are a good fit for the TSP.
TSP is an NP-hard problem, and there are no optimal TSP solvers that can handle large datasets.
But an evolutionary algorithm has no problems with this, this comes at the cost of only having an approximation of the optimal solution.
\\
This method of solving problems is another tool in my toolbox for solving problems.
I find the theory interesting, more and more computer science problems are being solved through algorithms inspired by nature (like deep learning mimicking the human brain).
Just using a simple heuristic and basic operators, the algorithm got further than I anticipated.

Another take away is that python might not be the best language for writing efficient code.

\end{document}
