#!/usr/bin/python

import Reporter
import numpy as np
from numba import prange, njit, int32, float64,deferred_type, gdb, objmode
from numba.typed import List, Dict
import numba.types as types
import time
import sys
import csv
import matplotlib.pyplot as plt
import math
import os

sys.setrecursionlimit(5000)

distanceMatrix = np.array([])
numCities = 0


#CONFIG

maxSame = 10
populationSize = 180
offspringSize = 25
timeLeft = 5*60
STAGE = 0

arguments = sys.argv

class r0735128:
    def __init__(self):
        self.reporter = Reporter.Reporter(self.__class__.__name__)

    def optimize(self, filename):
        global distanceMatrix
        global numCities
        global timeLeft
        global maxSame
        global STAGE

        file = open(filename)
        distanceMatrix = np.loadtxt(file, delimiter=",", dtype='f8')
        file.close()
        numCities = len(distanceMatrix)

        maxSame = math.log(numCities + 1,10) * 70

        mainGA = MAIN()
        iteration = 0
        while( not mainGA.stop ):
            mainGA.tick()
            best = np.inf
            mean = []
            bestPath = []
            for ga in mainGA.gaList:
                best_i = getBestPathIndex(ga.populationMatrix)
                if calculateFitnessPath(ga.populationMatrix[best_i]) < best:
                    best = calculateFitnessPath(ga.populationMatrix[best_i])
                    bestPath = ga.populationMatrix[best_i].copy()
            for p in mainGA.gaList[-1].populationMatrix:
                val = calculateFitnessPath(p)
                if val != np.inf:
                    mean.append(val)
            
            mean = np.mean(mean)
            timeLeft = self.reporter.report(mean, best, bestPath)
            if iteration % 50 == 0:
                print("time: " + str(timeLeft))
                #print("STAGE: " + str(STAGE))
            if timeLeft < 0:
                break
            iteration += 1

        return 0

@njit
def calculateFitnessPath(p):
    fitness = 0
    for j in range(numCities - 1):
        fitness += distanceMatrix[p[j], p[j+1]]
    return fitness + distanceMatrix[p[-1], p[0]]

@njit
def selectClassic(offspringSize, populationMatrix):
    selected = List()
    i = 0
    while(i < offspringSize):
        best_i = kTournament(populationMatrix, 3)
        if(not best_i in selected):
            selected.append(best_i)
            i+=1
    return selected

@njit
def kTournament(populationMatrix, size, exclude=np.array([], dtype='i4'), reverse=False):
    choice = np.arange(len(populationMatrix))
    if len(exclude) > 0:
        choice = np.delete(choice, exclude)
    best = np.random.choice(choice)
    for i in np.random.choice(choice, size=size):
        if(reverse):
            if( calculateFitnessPath(populationMatrix[i]) > calculateFitnessPath(populationMatrix[best]) ):
                best = i
                choice = np.append(choice, i)
        else:
            if( calculateFitnessPath(populationMatrix[i]) < calculateFitnessPath(populationMatrix[best]) ):
                best = i
                choice = np.append(choice, i)
    return best
   
@njit(parallel = True)
def variate(populationMatrix, selected, mutationProbability):
    newPop = np.full(shape=(len(populationMatrix) + len(selected) -1 ,numCities), fill_value=-1, dtype='i4')
    for i in range(0, len(populationMatrix)):
        newPop[i] = populationMatrix[i]
    newPop = variate2(newPop, populationMatrix, selected)
    for i in prange(0,len(newPop)):
        p = newPop[i]
        p = np.roll(p, np.random.randint(0, numCities)) # Rotate list
        if (np.random.uniform(0,1) < mutationProbability):
            p1 = mutateSwap(p)
            p2 = mutateRSM(p)
            if calculateFitnessPath(p1) > calculateFitnessPath(p2):
                p1 = p2
            p = p1
                
        newPop[i] = p

    return newPop

@njit(parallel = True)
def variate2(newPop, populationMatrix, selected):
    for i in prange(0, len(selected)-1):
        i1 = selected[i]
        i2 = selected[i+1]
        cross = crossoverBCSCX(populationMatrix[i1], populationMatrix[i2])
        newPop[i + len(populationMatrix)] = cross
    return newPop


maxTries = 10
@njit
def mutateRSM(p):
    og=p.copy()
    i1 = np.random.randint(0, numCities-1)
    i2 = np.random.randint(i1, numCities)
    p[i1:i2] = p[i1:i2][::-1]
    tries = 0
    while calculateFitnessPath(p) == np.inf:
        p = og.copy()
        i1 = np.random.randint(0, numCities-1)
        i2 = np.random.randint(i1, numCities)
        p[i1:i2] = p[i1:i2][::-1]
        tries += 1
        if tries >= maxTries:
            p = og
            break
    return p

@njit
def mutateSwap(p):
    og = p.copy()
    j1 = np.random.randint(numCities)
    j2 = np.random.randint(numCities)
    p[j1], p[j2] = p[j2], p[j1]
    tries = 0
    while calculateFitnessPath(p) == np.inf:
        j1 = np.random.randint(numCities)
        j2 = np.random.randint(numCities)
        p = og.copy()
        p[j1], p[j2] = p[j2], p[j1]
        tries += 1
        if tries >= maxTries:
            p = og
            break

    return p

@njit
def crossoverBCSCX(p, q):
    newPath = []
    newPath.append(np.random.randint(0, numCities))
    while len(newPath) < numCities:
        possible = []
        for j in range(0, numCities):
            if p[j] == newPath[-1]:
                if p[j-1] not in newPath and p[j-1] in possibleNextNodes(newPath[-1]):
                    possible.append(p[j-1])

                if j+1 < numCities:
                    if p[j+1] not in newPath and p[j+1] in possibleNextNodes(newPath[-1]):
                        possible.append(p[j+1])
                else:
                    if p[0] not in newPath and p[0] in possibleNextNodes(newPath[-1]):
                        possible.append(p[0])
                break

        for j in range(0, numCities):
            if q[j] == newPath[-1]:
                if q[j-1] not in newPath and q[j-1] in possibleNextNodes(newPath[-1]):
                    possible.append(q[j-1])

                if j+1 < numCities:
                    if q[j+1] not in newPath and q[j+1] in possibleNextNodes(newPath[-1]):
                        possible.append(q[j+1])
                else:
                    if q[0] not in newPath and q[0] in possibleNextNodes(newPath[-1]):
                        possible.append(q[0])
                break

        if len(possible) > 0:
            best = possible[0]
            for j in possible[1:]:
                if(distanceMatrix[newPath[-1], j] < distanceMatrix[newPath[-1], best]):
                    best = j
            newPath.append(best)
        else:
            rand = possibleNextNodes(newPath[-1])
            np.random.shuffle(rand)
            best = rand[0]
            if best in newPath:
                for n in rand[1:]:
                    if n not in newPath:
                        best = n
                        break
            if best in newPath:
                for n in range(0, numCities):
                    if not n in newPath:
                        best = n
                        break
            newPath.append(best)

    return newPath 

@njit(parallel = True)
def localSearchFirstFast(populationMatrix):
    stop = int(len(populationMatrix)/20) + 1
    for n in prange(0,stop):
        k = np.random.randint(len(populationMatrix))
        p = localSearch(populationMatrix[k].copy())
        if calculateFitnessPath(p) < calculateFitnessPath(populationMatrix[k]):
            populationMatrix[n] = p
    return populationMatrix

@njit
def localSearch(best):
    for i1 in range(0,numCities-1):
        for i2 in range(i1+1, numCities):
            p = best.copy()
            p[i1:i2] = p[i1:i2][::-1]
            if calculateFitnessPath(p) < calculateFitnessPath(best):
                best = p.copy()
    return best
            
@njit
def getBestPathIndex(populationMatrix):
    best = 0
    for i in range(1, len(populationMatrix)):
        if calculateFitnessPath(populationMatrix[best]) > calculateFitnessPath(populationMatrix[i]):
            best = i
    return best

def initializationHam(populationSize):
    populationMatrix = np.full(shape=(populationSize,numCities), fill_value=-1)
    for i in range(0, populationSize):
        populationMatrix[i] = hamCycle()
    return populationMatrix


@njit
def possibleNextNodes(i):
    return np.where((distanceMatrix[i] != np.inf) & (distanceMatrix[i] != 0))[0]

def isSafe(v, pos, path):
    if distanceMatrix[ path[pos-1] ][v] == np.inf:
        return False

    for vertex in path:
        if vertex == v:
            return False

    return True

dictSorted = {}
def hamCycleUtil(path, pos):
    if pos == numCities:
        if distanceMatrix[ path[pos-1] ][ path[0] ] != np.inf:
            return True
        else:
            return False

    if path[-1] in dictSorted.keys():
        l = dictSorted[path[-1]]
    else:
        l = list(np.arange(numCities))
        l.sort(key=lambda i: distanceMatrix[path[-1], i])
        dictSorted[path[-1]] = l
    '''
    l = np.arange(numCities)
    np.random.shuffle(l)
    '''
    for v in l:
        if isSafe(v, pos, path) == True:

            path[pos] = v

            if hamCycleUtil(path, pos+1) == True:
                return True

            path[pos] = -1

    return False

# Based on https://www.geeksforgeeks.org/hamiltonian-cycle-backtracking-6/
def hamCycle():
    l = np.arange(numCities)
    per = np.random.permutation(l)
    if calculateFitnessPath(per) != np.inf:
        return per

    path = [-1] * numCities
    path[0] = np.random.randint(numCities)

    if hamCycleUtil(path,1) == False:
        print ("Solution does not exist\n")
        return False

    return path

class GA:
    numberOfMerges = 0
    def __init__(self, populationSize, offspringSize, mutationProbability):
            self.iteration = 0
            self.mutationProbability = mutationProbability
            self.populationSize = populationSize
            self.offspringSize = offspringSize
            self.populationMatrix = initializationHam(self.populationSize)

    prevBestPathLength = 0
    def tick(self):
        #Elitism
        best_i = getBestPathIndex(self.populationMatrix)
        bestPath = self.populationMatrix[best_i].copy()
        bestPathLength = calculateFitnessPath(bestPath)

        selected = selectClassic(self.offspringSize+1, self.populationMatrix)

        newPop = variate(self.populationMatrix, selected, self.mutationProbability)
        for p in newPop:
            l = np.full(numCities, fill_value=-1)
            for n in p:
                l[n] = 0
            assert np.sum(l) == 0


        newPop = np.roll(newPop,np.random.randint(0, len(newPop)), axis=0)

        newPopOG = newPop.copy()
        newPop = localSearchFirstFast(newPop)
        for i in range(0, len(newPop)): # Strage parallelism fix, TODO
            p = newPop[i]
            l = np.full(numCities, fill_value=-1)
            for n in p:
                l[n] = 0
            if np.sum(l) != 0:
                newPop[i] = newPopOG[i].copy()

        selected = selectClassic(self.populationSize-1, newPop)

        newPop = newPop[selected]

        #Elitism
        newPop = np.vstack(( newPop, bestPath ))
        self.prevBestPathLength = bestPathLength

        '''
        # Check dat de populatie geldige paden bevat
        for p in newPop:
            l = np.full(numCities, fill_value=-1)
            for n in p:
                l[n] = 0
            assert np.sum(l) == 0
        '''

        self.populationMatrix = newPop

        self.iteration += 1


@njit
def greedySinglePath(bestPath):
    for i1 in range(0, numCities+1):
        for i2 in range(0, i1):
            p = bestPath.copy()
            p[i2:i1] = p[i2:i1][::-1]
            if calculateFitnessPath(p) < calculateFitnessPath(bestPath):
                bestPath = p.copy()
    return bestPath

class MAIN:
    gaList = []
    nbOfMigrating = offspringSize
    iteration = 1
    same = 0
    prevBest = -1
    prevBestGA = 0
    firstMigrate = True
    convergenceTime = 0
    stop = False

    def __init__(self):
        self.gaList = []
        self.iteration = 1
        self.same = 0
        self.prevBest = -1
        self.firstMigrate = True
        print("Creating the first GA")
        ga = GA(
            populationSize,
            offspringSize,
            0.1)
        print("Done")
        self.gaList.append(ga)
        self.convergenceTime = timeLeft
        
    def tick(self):
        currGa = self.gaList[-1]

        currGa.tick()
        best_i = getBestPathIndex(currGa.populationMatrix)
        if abs(calculateFitnessPath(currGa.populationMatrix[best_i]) - self.prevBest) < 1: # Because floats...
            self.same += 1
        else:
            if self.same >= 2:
                self.same -= 2
        self.prevBest = calculateFitnessPath(currGa.populationMatrix[best_i])

        currGa.mutationProbability = 0.5 - (min(0.4, (maxSame - self.same)/maxSame ))

        if self.iteration % 50 == 0:
            print("----"+str(self.iteration)+"----")
            for i in range(0, len(self.gaList)):
                ga = self.gaList[i]
                print("====FOR GA: " + str(i) + "====")
                best = ga.populationMatrix[0]
                for p in ga.populationMatrix:
                    if calculateFitnessPath(best) > calculateFitnessPath(p):
                        best = p
                print("best: " + str(calculateFitnessPath(best)))
                print("muta: " + str(ga.mutationProbability))
                print("nbMe: " + str(ga.numberOfMerges))
            print("same: " + str(self.same))

        if (self.same == int(3*maxSame/4)) and len(self.gaList) > 1:
            if self.firstMigrate:
                self.same = 0
            self.firstMigrate = False
            print("Migrating...")
            migrating = int(self.nbOfMigrating/len(self.gaList))
            if migrating == 0:
                migrating = 1

            currGa.numberOfMerges += 1
            selectedFitness = []
            for i in range(0, len(self.gaList)-1):
                ga = self.gaList[i]
                s = np.random.randint(len(ga.populationMatrix), size=migrating)
                selected = ga.populationMatrix[s]
                go = []
                for path in selected:
                    val = calculateFitnessPath(path)
                    if val != np.inf and not int(val) in selectedFitness:
                        selectedFitness.append(int(val))
                        go.append(path)

                if len(go) > 0:
                    currGa.populationMatrix = np.vstack(( currGa.populationMatrix, go ))

        if self.same >= maxSame:
            self.convergenceTime = self.convergenceTime - timeLeft
            if self.convergenceTime*1.1 < timeLeft:
                if len(self.gaList) >= 2:
                    best1 = calculateFitnessPath(self.gaList[-1].populationMatrix[getBestPathIndex(self.gaList[-1].populationMatrix)])
                    best2 = calculateFitnessPath(self.gaList[-2].populationMatrix[getBestPathIndex(self.gaList[-2].populationMatrix)])
                    if int(best1) == int(best2):
                        self.prevBestGA += 1
                        if self.prevBestGA > 5:
                            self.stop = True
                print("Creating new GA")
                self.convergenceTime = timeLeft
                ga = GA(
                    populationSize,
                    offspringSize,
                    0.1
                    )
                self.gaList.append(ga)
                self.same = 0
                self.firstMigrate = True
            else:
                print("Not enough time for another GA")
                self.same = 0
        
        self.iteration = self.iteration + 1


'''
nbTests = 15
if len(arguments) > 2:
    means = []
    bests = []
    STAGE = 0
    if arguments[2] == "old":
        file1 = open('means.txt', 'r')
        file2 = open('bests.txt', 'r')
        lines = file1.readlines()
        lines2 = file2.readlines()
        for line in lines:
            l = line.strip()
            if l == '':
                continue
            means.append(float(l))
        for line in lines2:
            l = line.strip()
            if l == '':
                continue
            bests.append(float(l))

        file1.close()
        file2.close()
    else:
        for i in range(0, 1000):
            STAGE = i
            r = r0735128()
            r.optimize("tour29.csv")
            filecsv = open('r0735128.csv')
            csvreader = csv.reader(filecsv)
            next(csvreader)
            header = []
            header = next(csvreader)
            rows = []
            for row in csvreader:
                rows.append(row)

            row = rows[-1]
            iteration = int(row[0])
            time = float(row[1])
            mean = float(row[2])
            best = float(row[3])
            cycle = '|'.join(row[4:])
            means.append(mean)
            bests.append(best)
            filecsv.close()

            file_object = open('means.txt', 'a')
            file_object.write("\n" + str(mean))
            file_object.close()
            file_object = open('bests.txt', 'a')
            file_object.write("\n" + str(best))
            file_object.close()

    plt.hist(means, 50, density=True, facecolor='r', alpha=0.75)
    plt.xlabel('Fitness')
    plt.ylabel('Frequency')
    plt.title('Histogram of mean objective values for the tour29 dataset')
    plt.grid(True)
    plt.savefig('meanhist.png', dpi=600)

    plt.figure().clear()
    plt.close()
    plt.cla()
    plt.clf()

    plt.hist(bests, 50, density=True, facecolor='r', alpha=0.75)
    plt.xlabel('Fitness')
    plt.ylabel('Frequency')
    plt.title('Histogram of best objective values for the tour29 dataset')
    plt.grid(True)
    plt.savefig('besthist.png', dpi=600)

    print("MEANS")
    print("STD:")
    print(np.std(means))
    print("MEAN:")
    print(np.mean(means))
    print("BESTS")
    print("STD:")
    print(np.std(bests))
    print("MEAN:")
    print(np.mean(bests))


else:
    #for file in ["tour29.csv", "tour100.csv", "tour250.csv", "tour500.csv", "tour750.csv", "tour1000.csv"]:
    for file in [arguments[1]]:
        tests = []
        distanceMatrix = np.array([])
        numCities = 0
        for i in range(0, nbTests):
            tests.append([])
            filenameout = 'r0735128' + file + '.' + str(i) + '.csv'
            if not os.path.exists(filenameout):
                fileout = open(filenameout, 'w')
                writer = csv.writer(fileout)

                print("start iter " + str(i) + " for " + file)
                r0735128().optimize(file)
                print("end iter " + str(i) + " for "+ file)

                filecsv = open('r0735128.csv')

                csvreader = csv.reader(filecsv)
                next(csvreader)
                header = []
                header = next(csvreader)
                rows = []

                for row in csvreader:
                    rows.append(row)
                    iteration = int(row[0])
                    time = float(row[1])
                    mean = float(row[2])
                    best = float(row[3])
                    cycle = '|'.join(row[4:])
                    writer.writerow([iteration, mean, best, cycle])
                    tests[i].append([iteration, mean, best, cycle])
                filecsv.close()
                fileout.close()
            else:
                filecsv = open(filenameout)
                csvreader = csv.reader(filecsv)
                rows = []
                for row in csvreader:
                    rows.append(row)
                    iteration = int(row[0])
                    mean = float(row[1])
                    best = float(row[2])
                    cycle = row[3]
                    tests[i].append([iteration, mean, best, cycle])
                filecsv.close()

        minIterations = np.inf
        means = []
        bests = []
        for test in tests:
            if minIterations > test[-1][0]:
                minIterations = test[-1][0]
            
        for i in range(0, nbTests):
            test = tests[i]
            means.append([])
            bests.append([])
            for iteration in test[0:minIterations+1]:
                means[i].append(iteration[1])
                bests[i].append(iteration[2])

            if i == 0:
                plt.plot(range(0, minIterations+1), means[i][0:minIterations+1], color='darkred', alpha=0.5, label="Mean objective function")
                plt.plot(range(0, minIterations+1), bests[i][0:minIterations+1], color='darkblue', alpha=0.5, label='Best objective function')
            else:
                plt.plot(range(0, minIterations+1), means[i][0:minIterations+1], color='darkred', alpha=0.5)
                plt.plot(range(0, minIterations+1), bests[i][0:minIterations+1], color='darkblue', alpha=0.5)

        stdDevMeans = np.std(means, axis=0)
        stdDevBests = np.std(bests, axis=0)

        meanMeans = np.array(means).mean(axis=0)
        meanBests = np.array(bests).mean(axis=0)

        plt.plot(range(0, minIterations+1), meanMeans[0:minIterations+1], color='red', linewidth=5.0, label="Mean of all the mean objective funtions")
        plt.plot(range(0, minIterations+1), meanBests[0:minIterations+1], color='blue', linewidth=5.0, label="Mean of all the best objective functions")

        #plt.fill_between(range(0, minIterations+1), meanMeans[0:minIterations+1] + stdDevMeans[0:minIterations+1], meanMeans[0:minIterations+1] - stdDevMeans[0:minIterations+1], facecolor='red', alpha=0.3)

        #plt.fill_between(range(0, minIterations+1), meanBests[0:minIterations+1] + stdDevBests[0:minIterations+1], meanBests[0:minIterations+1] - stdDevBests[0:minIterations+1], facecolor='blue', alpha=0.3)

        #plt.text(0.56, 0.55, ' end mean value={} \n end best value={}'.format(round(meanMeans[-1],2), round(np.min(np.array(bests).min(axis=0)),2)), transform=plt.gca().transAxes) # monster regel

        plt.xlabel("Iterations")
        plt.ylabel("Objective value")
        plt.title("Individual phase GA performance for " + file)

        plt.legend()
        plt.savefig(file + '.png', dpi=600)
        #plt.show()
        print("done " + file)
'''
